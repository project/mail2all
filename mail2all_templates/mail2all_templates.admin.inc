<?php

/**
 * Helper function to validate if the element is correctly entered
 */
function _mail2all_templates_validate_field($element) {
  switch ($element['#name']) {
    case 'name':
      if (!preg_match('!^[a-z0-9_]+$!', $element['#value'])) {
        form_error($element, t('The machine-readable name must contain only lowercase letters, numbers, and underscores.'));
      }
    break;
  }
}

/**
 * Callback to show a list of templates
 */
function mail2all_templates_form() {
   $header = array(
    array(
      'data' => t('Name'),
      'field' => 'name',
    ),
    array(
      'data' => t('Description'),
      'field' => 'description',
    ),
    array(
      'data' => t('File suggested'),
      'field' => 'file-suggested',
    ),
    array(
      'data' => t('Details'),
      'field' => 'datails',
    ),
  );
  $sql = "SELECT * FROM {mailtoall_templates}";
  $sql .= tablesort_sql($header);
  
  $result = pager_query($sql, 50, 0, NULL);
  $rows = array();
  while($data = db_fetch_object($result)) {
    $row = array(
      $data->name,
      $data->description,
      'mimemail-message--' . $data->name . '.tpl.php',
      l(t("edit"), 'admin/settings/mail2all/templates/edit/' . $data->name),
    );
    
    $rows[] = $row;
  }
  
  if (empty($rows)) {
    $empty_message = t('No templates created, !create', array('!create' => l(t("create one"), 'admin/settings/mail2all/templates/new')));
    $rows[] = array(array(
        'data' => $empty_message,
        'colspan' => 7,
    ));
  }
  
  return theme("mail2all_templates", $header, $rows);
}

/**
 * Theme function to wrap the table from mail2all_templates_form and not
 * produce any html output there
 */
function theme_mail2all_templates($header, $rows) {
  $output .= t("Templates are used so you can customize the output of your emails. You could have multiple templates and configure them differently. Just create a template and then with the suggested filename go to your theme directory en create the file. After that you can edit the file");
  
  $output .= '<br /><br />' . l(t("+ Create a new template"), 'admin/settings/mail2all/templates/new');
  $output .= '<br /><br />' . theme('table', $header, $rows);
  $output .= theme('pager', NULL, 50, 0);
  
  return $output;
}

/**
 * Form callback to add and edit a template
 */
function mail2all_templates_template_form($form, $name = NULL) {
  $form = array();
  
  global $user;
  
  $template = mail2all_templates_load($name);
 
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t("Template Name"),
    '#description' => t("Enter a machine name for this template"),
    '#default_value' => isset($template['name']) ? $name : $template['name'],
    '#element_validate' => isset($template['name']) ? NULL  : array('_mail2all_templates_validate_field'),
    '#weight' => -10,
    '#disabled' => isset($template['name']) ? TRUE : FALSE,
  );
  
  if($template['name']) {
    $form['edit_name'] = array(
      '#type' => 'hidden',
      '#value' => $template['name'],
    );
  }
  
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t("Template Description"),
    '#required' => TRUE,
    '#description' => t("Enter a description for this template, this will be the name used when you select a template"),
    '#default_value' => isset($template['description']) ? $template['description'] : '',
    '#weight' => -9,
  );
  
  $form['actions'] = array(
    '#prefix' => '<div class="submit-actions">',
    '#suffix' => '</div>',
    '#weight' => 100,
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save"),
  );
  
  return $form;
}

/**
 * Submit callback for mail2all_templates_template_form()
 * 
 * @see mail2all_templates_template_form()
 */
function mail2all_templates_template_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $template = new stdClass();
  $template->description = $values["description"];
  
  // Insert
  if(!isset($values['edit_name'])) {
    $template->name = $values['name'];
    mail2all_templates_insert($template);
  }
  // Update
  else {
    $template->name = $values['edit_name'];
    mail2all_templates_update($template);
  }
  
  drupal_set_message(t("Template %template saved", array('%template' => $template->description)));
  drupal_goto("admin/settings/mail2all/templates");
}


