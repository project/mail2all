<?php

/**
 * Page callback to see history of emails sent
 */
function mail2all_history() {
  $header = array(
    array(
      'data' => t('Status'),
      'field' => 'status',
    ),
    array(
      'data' => t('From'),
      'field' => 'author',
    ),
    array(
      'data' => t('Subject'),
      'field' => 'subject',
    ),
    array(
      'data' => t('Template'),
      'field' => 'template',
    ),
    array(
      'data' => t('Roles'),
      'field' => 'roles',
    ),
    array(
      'data' => t('Sent by'),
      'field' => 'uid',
    ),
    array(
      'data' => t('Sent on'),
      'field' => 'timestamp',
    	'sort' => 'asc',
    ),
    array(
      'data' => t('Details'),
      'field' => 'datails',
      'colspan' => user_access("resend email to all users") ? "3" : "1",
    ),
  );
  $sql = "SELECT * FROM {mailtoall}";
  $sql .= tablesort_sql($header);
  
  $result = pager_query($sql, 25, 0, NULL);
  $status = mail2all_status();
  $rows = array();
  while($data = db_fetch_object($result)) {
    $row = array(
      $status[$data->status],
      $data->author,
      l($data->subject, 'admin/content/mail2all/' . $data->mid),
      $data->template,
      $data->roles,
      user_load($data->uid)->name,
      format_date($data->timestamp),
      l(t("view details"), 'admin/content/mail2all/' . $data->mid),
      l(t("log"), 'admin/content/mail2all/history/' . $data->mid),
      user_access("resend email to all users") ? l(t("send again"), 'admin/content/mail2all/sendagain/' . $data->mid) : NULL,
    );
    
    $rows[] = $row;
  }
  
  if (empty($rows)) {
    $empty_message = t('No mails created');
    $rows[] = array(array(
        'data' => $empty_message,
        'colspan' => 7,
    ));
  }
  
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 25, 0);

  return $output;
}

/**
 * Returns a page for a single mail registered in the database
 * 
 * @param $mid int the unique identifier of the mail
 * 
 */
function mail2all_single_mail($mid) {
  drupal_add_css(drupal_get_path("module", "mail2all") . '/mail2all.css');
  
  $status = mail2all_status();
  $result = db_query("SELECT * FROM {mailtoall} m WHERE m.mid = %d", $mid);
  while($data = db_fetch_object($result)) {
    $mail = new stdClass();
    $mail->mid = $data->mid;
    $mail->timestamp = $data->timestamp;
    $mail->uid = $data->uid;
    $mail->from = $data->author;
    $mail->subject = $data->subject;
    $mail->body = check_markup($data->body, $data->format);
    $mail->roles = $data->roles;
    $mail->status = $status[$data->status];
    $mail->format = $data->format;
    $mail->footer = $data->footer;
  }
  
  drupal_set_title(t("Email %subject sent on %time", array('%subject' => $mail->subject, '%time' => format_date($mail->timestamp))));
  return theme("maildetail", $mail);
}

/**
 * Send again 
 */
function mail2all_send_again($form, $mid) {
  $form = array();
  
  $form['mid'] = array(
    '#value' => $mid,
    '#type' => 'hidden',
  );
  
  $form['message'] = array(
    '#value' => t("Are you sure you want to send this email again?"),
    '#type' => 'markup',
  );
  
  $form['actions'] = array(
    '#prefix' => '<div class="submit-actions">',
    '#suffix' => '</div>',
    '#weight' => 100,
  );
  
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Yes"),
    '#submit' => array("mail2all_send_again_submit_send"),
  );
  
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t("No, cancel"),
    '#submit' => array("mail2all_send_again_submit_cancel"),
  );
  
  return $form;
}

/**
 * Submit callback to send an email again
 */
function mail2all_send_again_submit_send(&$form, &$form_state) {
  $values = $form_state['values'];
  
  $mid = $values['mid'];
  
  $result = db_query("SELECT * FROM {mailtoall} m WHERE m.mid = %d", $mid);
  while($data = db_fetch_object($result)) {
    $mail = new stdClass();
    $mail->timestamp = time();
    $mail->uid = $data->uid;
    $mail->from = $data->author;
    $mail->subject = $data->subject;
    $mail->template = $data->template;
    $mail->body = $data->body;
    $mail->roles = $data->roles;
    $mail->status = MAIL2ALL_QUEUE;
    $mail->format = $data->format;
    $mail->footer = $data->footer;
  }
    
  mail2all_insert($mail);
  
  drupal_set_message(t("The message %mail has been programmed to be sent again", array('%mail' => $mail->subject)));
  drupal_goto("admin/content/mail2all");
}

/**
 * Submit callback to cancel
 */
function mail2all_send_again_submit_cancel(&$form, &$form_state) {
  drupal_goto("admin/content/mail2all");
}


/**
 * See the log page for emails sent
 */
function mail2all_queue_log_page($mid) {
  $mail = mail2all_load_mail($mid);
  $header = array(
    array(
      'data' => t('Status'),
      'field' => 'status',
    ),
    array(
      'data' => t('User Id'),
      'field' => 'uid',
    ),
    array(
      'data' => t('Email'),
      'field' => 'mail',
    ),
    array(
      'data' => t('Created'),
      'field' => 'created',
    ),
    array(
      'data' => t('Sent'),
      'field' => 'sent',
    ),
  );
  $sql = "SELECT m.*, u.mail FROM {mailtoall_queue} m LEFT JOIN {users} u ON m.uid = u.uid WHERE mid = %d";
  $sql .= tablesort_sql($header);
  
  $result = pager_query($sql, 25, 0, NULL, $mail["mid"]);
  $status = mail2all_status_queue();
  $rows = array();
  while($data = db_fetch_object($result)) {
    $row = array(
      $status[$data->status],
      $data->uid,
      $data->mail,
      format_date($data->created),
      !empty($data->sent) ? format_date($data->sent) : t("Waiting"),
    );
    
    $rows[] = $row;
  }
  
  if (empty($rows)) {
    $empty_message = t('No data available');
    $rows[] = array(array(
        'data' => $empty_message,
        'colspan' => 5,
    ));
  }
  
  $output .= theme('table', $header, $rows);
  $output .= theme('pager', NULL, 25, 0);

  return $output;
}