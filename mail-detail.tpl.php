<div class="field mail2all-mail">
  <div class="mail2all-mail-data">
    
    <div class="mail2all-field mail2all-field-from">
      <div class="mail2all-label">
        <?php print t("From: "); ?>
      </div>
      <?php print $mail->from; ?>
    </div>
    
    <div class="mail2all-field mail2all-field-subject">
      <div class="mail2all-label">
        <?php print t("Subject: "); ?>
      </div>
      <?php print $mail->subject; ?>
    </div>
    
    <div class="mail2all-field mail2all-field-date">
      <div class="mail2all-label">
        <?php print t("Date created: "); ?>
      </div>
      <?php print format_date($mail->timestamp); ?>
    </div>
    
    <div class="mail2all-field mail2all-field-status">
      <div class="mail2all-label">
        <?php print t("Status: "); ?>
      </div>
      <?php print $mail->status; ?>
    </div>
  </div>
  
  <div class="mail2all-field-body">
    <?php print $mail->body; ?>
  </div>
  
  <div class="mail2all-field-footer">
    <?php print $mail->footer; ?>
  </div>
  
  <?php print $send_again; ?>
</div>