<?php print '<h3 class="preview-title">' . t("Preview") . '</h3>'; ?>
<div class="field mail2all-mail">
  <div class="field-items">
    <div class="field-label-inline-first">
      <?php print t("Send options: "); ?>
    </div>
    <?php print $mail->queue; ?>
  </div>
  
  <div class="field-items">
    <div class="field-label-inline-first">
      <?php print t("From: "); ?>
    </div>
    <?php print $mail->from; ?>
  </div>
  
  <div class="field-items">
    <div class="field-label-inline-first">
      <?php print t("Template: "); ?>
    </div>
    <?php print $mail->template; ?>
  </div>
  
  <div class="field-items">
    <div class="field-label-inline-first">
      <?php print t("Subject: "); ?>
    </div>
    <?php print $mail->subject; ?>
  </div>
  
  <div class="field-label">
    <?php print t("Body: "); ?>
  </div>
  <div class="field-items">
    <?php print $mail->body; ?>
  </div>
  
  
  <div class="field-items">
    <div class="field-label-inline-first">
      <?php print t("Footer: "); ?>
    </div>
    <?php print $mail->footer; ?>
  </div>
</div>


