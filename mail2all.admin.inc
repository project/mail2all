<?php

/**
 * Callback used for configuration
 */
function mail2all_admin_page($form) {
  $form = array();
  
  $form['mail2all_footer'] = array(
    '#type' => 'textarea',
    '#title' => t("Footer text"),
    '#default_value' => variable_get("mail2all_footer", t("This is an auto-generated email. Please do not respond to it. You're receiving this email because you have an account registered at !site_name")),
    '#rows' => 5,
    '#description' => t("This is the default message that will be used as footer. The user creating the email might change it. Allowed tokens are: !realname, !username, !email, !uid, !site_name and !site_url")
  );
  
  $form['mail2all_test'] = array(
    '#type' => 'textfield',
    '#title' => t("User to receive test"),
    '#default_value' => variable_get("mail2all_test", NULL),
    '#description' => t("This is the user that will be used to receive tests"),
    '#autocomplete_path' => 'user/autocomplete',
  );
  
  $type = module_exists("mimemail") ? 'html' : "plaintext";
  $form['mail2all_type'] = array(
    '#type' => 'select',
    '#title' => t("Send mails using"),
    '#options' => array(
      'html' => t("HTML"),
      'plaintext' => t("Plain text"),
    ),
    '#default_value' => variable_get("mail2all_type", $type),
    '#description' => t("A site might have installed mimemail and configure it to not send emails, therefore, is neccesary to set here HTML if you want to use it. Also, a site could have mimemail configured to send all emails with HTML but you could want to send mail2all emails using plain text. This option allows you to do that and override mimemail configuration. If you select HTML and you create an email selecting no format at all, users will receive an email with code they can see."),
  );

  $form["mail2all_queue"] = array(
    '#type' => 'textfield',
    '#title' => t("Mails to send on every cron run"),
    '#default_value' => variable_get("mail2all_queue", 25),
    '#description' => t("You can set how many mails you want to send on each cron. Set this setting to a reasonable number because you don't want to be running a cron job for a long time and too many mails to send can cause to run cron for hours")
  );
  
  return system_settings_form($form);
}